import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Provider} from 'react-redux';
import BlogList from './app/BlogList';
import AddBlog from './app/AddBlog';

import store from './app/store';
import Icon from 'react-native-vector-icons/Ionicons';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>HomeScreen!</Text>
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

const Blog = createStackNavigator(
  {
    BlogList: {
      screen: BlogList,
      navigationOptions: {title: 'BLOG LIST'},
    },

    AddBlog: {screen: AddBlog, navigationOptions: {title: 'ADD NEW BLOG'}},
  },
  {
    initialRouteName: 'BlogList',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#3498db',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 14,
      },
    },
  },
);

const TabNavigator = createBottomTabNavigator({
  Blog: Blog,
  Home: HomeScreen,
  Settings: SettingsScreen,
});

const MyTab = createAppContainer(TabNavigator);

export default class App extends Component {
  constructor() {
    super();
    console.disableYellowBox = true;
  }
  render() {
    return (
      <Provider store={store}>
        <MyTab />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
