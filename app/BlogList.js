/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import * as BlogActions from './actions/BlogActions';

class BlogList extends Component {
  constructor() {
    super();
    this.state = {
      ID: '',
    };
  }
  componentDidMount() {
    // this.props.DeleteBlog();
    this.props.loadBlog({id: '12'});
  }
  render() {
    return (
      <View style={styles.container}>
        {this.props.isLoading ? (
          <ActivityIndicator />
        ) : (
          <View style={styles.Box}>
            <ScrollView>
              {this.props.Blog.map(data => {
                return (
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontSize: 14}}>{data.title}</Text>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.DeleteBlog(data.id);
                        }}
                        style={{fontSize: 14}}>
                        <Text style={{fontSize: 14}}>DELETE</Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        backgroundColor: '#f3f3f3',
                        marginTop: 10,
                        marginBottom: 10,
                        height: 1,
                        width: '100%',
                      }}></View>
                  </View>
                );
              })}
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.push('AddBlog');
                }}
                style={{
                  height: 40,
                  width: '100%',
                  backgroundColor: '#3498db',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#fff', fontWeight: 'bold'}}>
                  ADD NEW BLOG
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    Blog: state.Blog,
    isLoading: state.isLoading,
    error: state.error,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    loadBlog: () => dispatch(BlogActions.loadBlog()),
    DeleteBlog: id => dispatch(BlogActions.DeleteBlog(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BlogList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f3f3',
  },
  Box: {
    marginTop: 10,
    justifyContent: 'center',
    width: '94%',
    marginLeft: '3%',
    backgroundColor: '#fff',
    padding: 20,
    paddingBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
});
