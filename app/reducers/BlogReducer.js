let initialState = {
  Blog: [{id: 21, title: 'dsd', dis: 'ds'}],
  isLoading: false,
  error: null,
};

export default Blog = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_BLOG_START':
      return Object.assign({}, state, {isLoading: true});
    case 'LOAD_BLOG_SUCCESS':
      return Object.assign({}, state, {
        Blog: action.payload,
        isLoading: false,
      });
    case 'LOAD_BLOG_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false,
      });
    default:
      return state;
  }
};
