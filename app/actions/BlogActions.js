import axios from 'axios';

export const loadBlog = () => {
  return (dispatch, getState) => {
    dispatch({type: 'LOAD_QUOTE_START'});
    axios
      .get('http://konuze.com/test2/select.php')
      .then(function(response) {
        console.log(response);
        dispatch({type: 'LOAD_BLOG_SUCCESS', payload: response.data});
      })
      .catch(function(error) {
        dispatch({type: 'LOAD_BLOG_FAILURE', payload: error});
      });
  };
};
export const AddBlog = (TitleValue, Description, link) => {
  return (dispatch, getState) => {
    dispatch({type: 'LOAD_BLOG_START'});
    axios
      .get(
        'http://konuze.com/test2/insert.php?title=' +
          TitleValue +
          '&dis=' +
          Description +
          'link=3333',
      )
      .then(function(response) {
        dispatch({type: 'LOAD_BLOG_SUCCESS', payload: response.data});
      })
      .catch(function(error) {
        dispatch({type: 'LOAD_BLOG_FAILURE', payload: error});
      });
  };
};
export const DeleteBlog = ID => {
  return (dispatch, getState) => {
    dispatch({type: 'LOAD_BLOG_START'});
    axios
      .get('http://konuze.com/test2/delete.php?id=' + ID)
      .then(function(response) {
        dispatch({type: 'LOAD_BLOG_SUCCESS', payload: response.data});
      })
      .catch(function(error) {
        dispatch({type: 'LOAD_BLOG_FAILURE', payload: error});
      });

    this.loadBlog;
  };
};
