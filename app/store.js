import {createStore, applyMiddleware} from 'redux';
import BlogReducer from './reducers/BlogReducer';
import thunk from 'redux-thunk';

const store = createStore(BlogReducer, applyMiddleware(thunk));

export default store;
