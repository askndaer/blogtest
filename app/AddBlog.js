/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import * as BlogActions from './actions/BlogActions';

class BlogList extends Component {
  constructor() {
    super();
    this.state = {
      TitleValue: '',
      Description: '',
    };
  }

  AddBlog = () => {
    this.props.AddBlog(this.state.TitleValue, this.state.Description);
    if (this.props.error == null) this.props.navigation.pop();
  };

  componentDidMount() {}
  render() {
    return (
      <View style={styles.container}>
        {this.props.isLoading ? (
          <ActivityIndicator />
        ) : (
          <View style={styles.Box}>
            <ScrollView>
              <TextInput
                style={{
                  borderColor: '#f3f3f3',
                  fontSize: 14,
                  borderBottomWidth: 1,
                  marginBottom: 10,
                }}
                onChangeText={text => this.setState({TitleValue: text})}
                value={this.state.TitleValue}
                placeholder={'Title'}
              />
              <TextInput
                style={{
                  borderColor: '#f3f3f3',
                  fontSize: 14,
                  borderBottomWidth: 1,
                  marginBottom: 10,
                }}
                onChangeText={text => this.setState({Description: text})}
                value={this.state.Description}
                placeholder={'Description'}
              />

              <TouchableOpacity
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 40,
                  backgroundColor: '#3498db',
                  paddingTop: 10,
                }}
                onPress={this.AddBlog}>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: 14,
                    marginBottom: 10,
                    color: '#fff',
                    fontWeight: 'bold',
                  }}>
                  ADD
                </Text>
              </TouchableOpacity>
            </ScrollView>
            {/* <Button title="Load Quote" onPress={() => } /> */}
            {/* <Button title="Load Quote" onPress={() => this.props.AddBlog()} /> */}
          </View>
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    quote: state.quote,
    isLoading: state.isLoading,
    error: state.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadQuote: () => dispatch(BlogActions.loadQuote()),
    AddBlog: (TitleValue, Description) =>
      dispatch(BlogActions.AddBlog(TitleValue, Description, '3333')),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BlogList);

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: '#f3f3f3',
  },
  Box: {
    marginTop: 10,
    justifyContent: 'center',
    width: '94%',
    marginLeft: '3%',
    backgroundColor: '#fff',
    padding: 20,
    paddingBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
});
