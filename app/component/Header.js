/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class HEADER extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: 80,
          backgroundColor: '#3498db',
          justifyContent: 'flex-end',
        }}>
        <Text
          style={{
            // fontFamily: 'MyFont',
            alignSelf: 'center',
            fontSize: 14,
            marginBottom: 10,
            color: '#fff',
            fontWeight: 'bold',
          }}>
          {this.props.title}
        </Text>
      </View>
    );
  }
}
